# Introduction to Digitalization

Welcome to our project. We want to make the world a better place by educating brilliant minds about the Digital Era™.

## Usage

This project allows to jumpstart a new generation of the most talented minds in all of our Western Hemisphere. We are capable of turning your world view upside down, if you want to or not!

## Support

Please contact our Digitalization magician MAVN at marvin.milot@stud.sbg.ac.at for questions about our world-changing project that´s gonna change the way we see our complex society.

## Roadmap

- Research data for Primary Grade
- Research data for Secondary Grade
- Presenting our module in front of class
- Analyzing contributed data
- Presenting our findings in front of our colleagues

## Contributing

We are open for any good and constructive criticism. ༼ つ ◕_◕ ༽つ

## Authors and acknowledgment

Many thanks to Martschi, Flotschibär, Mavn, Martini

## License

© ¯\\_\_(ツ)_\_/¯

## Project status

(☞ﾟヮﾟ)☞
We are currently working really, really hard to make our vision work.
☜(ﾟヮﾟ☜)
The next step is to present our collected data about Artifical Intelligence.
